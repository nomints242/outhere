import styled from "styled-components";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { HeaderSubmenu } from "./HeaderSubmenu";

const Layout = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 999;

  @media (max-width: 899px) {
  }
`;

const HeaderLayout = styled.header`
  width: 100%;
  align-items: center;
  position: absolute;
  height: 60px;
  top: 32px;
  display: flex;
  z-index: 999;
  border-bottom: 1px solid #cbcac0;
  font-size: 12px;

  @media (max-width: 899px) {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 999;
    padding: 0.84375rem 1rem;
  }
`;

const LeftHeader = styled.div`
  display: flex;
  line-height: 60px;
  min-width: 130px;
  height: 60px;
  align-items: center;
  cursor: pointer;

  color: #e6eaea;
  .navlist {
    display: flex;
  }
  .show {
    border-right: 1px solid #e6eaea;
  }
  a {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0px 32px;
    svg {
      margin-right: 10px;
    }
  }
  .header_subMenu {
    display: none;
  }
  @media (max-width: 899px) {
    .leftList {
      display: none;
    }
    .header_subMenu {
      display: block;
      align-items: center;
      display: flex;
      justify-content: center;
    }
    button {
      width: 24px;
      height: 24px;
      padding: 0;
      border: 0;
      width: 1.5rem;
      background-color: transparent;
      cursor: pointer;
    }
    .header__menu-toggle-bar {
      margin: 5px 0;
      display: block;
      width: 1.5rem;
      height: 0.0625rem;
      background-color: #ffffff !important;
      transition: transform 0.2s ease;
    }
  }
`;

const RightHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  flex: 1;
  line-height: 60px;
  height: 60px;
  align-items: center;
  color: #e6eaea;
  .navlist {
    display: flex;
  }
  a {
    padding: 1.1875rem 1rem;
  }
  @media (max-width: 899px) {
    .navlink {
      display: none;
    }
    .user {
      display: block;

      padding: 0;
    }
  }
`;

const Logo = styled.div`
  position: absolute;
  left: 50%;
  width: auto;
  line-height: normal;
  transform: translateX(-50%);
`;

const SubHeader = styled.div`
  position: relative;
  font-size: 12px;
  color: #f3f4ed;
  text-align: center;
  align-items: center;
  height: 32px;
  background-color: #896d61;
  z-index: 999;
  p {
    line-height: 2rem;
    margin: 0;
  }
`;

const Header = () => {
  return (
    <Layout>
      <SubHeader>
        <p>Free shipping over $35, 60-day trial, guaranteed for life</p>
      </SubHeader>
      <HeaderLayout>
        <LeftHeader>
          <div className="header_subMenu">
            <button className="header__menu-toggle" type="button">
              <span className="header__menu-toggle-bar"></span>
              <span className="header__menu-toggle-bar"></span>
            </button>
          </div>
          <ul className="navlist leftList">
            <li>
              <a className="navlink show">Shop</a>
            </li>
            <li>
              <a className="navlink">
                <span>
                  Search
                  {/* <SearchRoundedIcon /> */}
                </span>
              </a>
            </li>
          </ul>
        </LeftHeader>
        <Logo>
          <a href="/#">
            <svg
              fill="none"
              height="26"
              viewBox="0 0 118 26"
              width="118"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                clip-rule="evenodd"
                d="M87.439 2.424C87.439 1.086 88.612 0 90.054 0c1.406 0 2.552 1.086 2.552 2.424 0 1.375-1.11 2.456-2.552 2.456S87.44 3.76 87.44 2.424zm26.655 22.234V5.226L118 2.246v22.412h-3.906zm-14.942-4.355c0 1.41 1.056 2.082 2.465 2.082 1.762 0 3.489-1.217 3.489-3.265v-2.076l-3.072.673c-1.154.281-2.882.858-2.882 2.586zm9.796.385c0 1.024.385 1.441 1.217 1.441h-.003c.255.006.508-.05.737-.161v2.626c-.666.28-1.381.42-2.103.413-2.048 0-3.265-1.024-3.585-2.913-1.217 2.02-2.882 3.043-4.962 3.043-2.692 0-4.87-1.696-4.87-4.608 0-3.65 3.396-4.547 5.284-4.9l4.413-.832v-.577c0-1.633-.705-2.785-2.691-2.785-1.824 0-2.848.929-3.104 2.21h-3.875c.738-2.882 3.299-4.932 7.268-4.932 4.098 0 6.274 1.954 6.274 5.349v6.626zm-17.033 3.97h-3.938V9.138h3.938v15.52zm-13.35-10.931V9.13h-3.88v15.52h3.97v-7.326c0-3.074 3.043-4.705 6.179-4.609V8.971c-2.782.16-4.797 1.405-6.27 4.756zm-11.211 1.39c-.224-1.953-1.314-3.586-3.49-3.586h.007c-2.05 0-3.395 1.345-3.715 3.587h7.198zm-3.58-6.404c4.706 0 7.364 3.747 7.364 8.101v.77H60.06c.102 2.88 1.632 4.61 3.906 4.61 1.436 0 2.688-.705 3.43-2.082.116-.205.212-.42.288-.64l3.293.735c-.044.129-.101.257-.163.396-.03.068-.063.139-.094.213-1.312 2.85-3.778 4.385-6.884 4.385-4.578 0-7.812-3.329-7.812-8.227 0-4.866 3.234-8.26 7.75-8.26zM49.527 19.696c0 1.665.675 2.112 1.922 2.112.865 0 1.76-.16 2.306-.415v2.943c-1.118.409-2.3.615-3.491.608-3.072 0-4.706-1.412-4.706-4.26v-8.838h-3.072v-2.71h3.072V6.673l3.97-2.985V9.13h3.97v2.716h-3.97v7.85zm-19.911.607c0 1.41 1.056 2.082 2.465 2.082 1.76 0 3.489-1.217 3.489-3.265v-2.076l-3.074.673c-1.152.281-2.88.858-2.88 2.586zm9.794.385c0 1.024.385 1.441 1.218 1.441h-.002c.255.006.508-.05.737-.161v2.626c-.666.28-1.382.42-2.103.413-2.05 0-3.266-1.024-3.587-2.913-1.215 2.02-2.88 3.043-4.962 3.043-2.693 0-4.869-1.696-4.869-4.608 0-3.65 3.393-4.547 5.282-4.9l4.412-.832v-.577c0-1.633-.71-2.785-2.688-2.785-1.826 0-2.85.929-3.106 2.21h-3.874c.737-2.882 3.297-4.932 7.267-4.932 4.098 0 6.275 1.954 6.275 5.349v6.626zm-26.857-8.741c1.028-2.016 2.688-3.264 4.832-3.264 2.914 0 4.932 1.984 4.932 5.698v10.277h-3.97v-9.83c0-1.983-.768-3.105-2.37-3.105-1.825 0-2.881 1.537-2.881 4.163v8.772H9.22v-9.83c0-1.983-.832-3.105-2.37-3.105-1.856 0-2.881 1.537-2.881 4.163v8.765H0V9.13h3.876v2.913c.798-2.113 2.463-3.361 4.48-3.361 2.018 0 3.555 1.12 4.196 3.264z"
                fill-rule="evenodd"
                fill="#ffffff"
              ></path>
            </svg>
          </a>
        </Logo>
        <RightHeader>
          <ul className="navlist rightList">
            <li>
              <a href="/#" className="navlink1">
                Gallery
              </a>
            </li>
            <li>
              <a href="/#" className="navlink1">
                About
              </a>
            </li>
            <li>
              <a href="/#" className="navlink1 cart">
                Cart
              </a>
            </li>
            <li>
              <a href="/account/login" className="navlink1 user">
                Account
              </a>
            </li>
          </ul>
          {/* <HeaderSubmenu /> */}
        </RightHeader>
      </HeaderLayout>
    </Layout>
  );
};

export default Header;
