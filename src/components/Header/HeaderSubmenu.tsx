import React, { useState } from "react";
import styled from "styled-components";

const Layout = styled.header`
  .dropdowns {
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    height: 100vh;
    display: grid;
    grid-template-columns: repeat(3, minmax(33.3333%, 1fr));
    opacity: 0;
    visibility: hidden;
  }
  .dropdowns_active {
    opacity: 1;
    visibility: visible;
  }

  .dropdown {
    position: relative;
    height: 100%;
    z-index: inherit;
    top: 0;
    width: 100%;
    left: 0;
    right: auto;
    transition: all 1s cubic-bezier(0.32, 0.24, 0.15, 1);
    transform: translate(-100%);
  }
  .dropdown1 {
    z-index: 3;
    background-color: #1d322d;
    color: #f3f4ed;
  }
  .dropdownInner {
    height: 100%;
  }
  .dropdownMenu {
    padding: 2.5rem 4rem;
    height: 100%;
    width: 30rem;
    .dropdownItem {
      padding: 0.5rem 0;
    }
    .dropdownItemLink {
      font-size: 1.625rem;
      line-height: 1.9375rem;
      letter-spacing: -0.04875rem;
      display: inline-flex;
      align-items: center;
    }
    .dropdownItemText {
      cursor: pointer;
    }
  }
`;

export function HeaderSubmenu() {
  const [isShown, setIsShown] = useState(false);
  return (
    <Layout>
      <div className="dropdowns dropdowns_active">
        <div className="dropdown dropdown1 dropdown_show">
          <div className="dropdownInner">
            <ul className="dropdownMenu">
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Shop all</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Sets</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Prepware</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Knives</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Cookware</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Tools</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Tabletop</span>
                </a>
              </li>
              <li className="dropdownItem">
                <a className="dropdownItemLink" href="">
                  <span className="dropdownItemText">Storage + Care</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="dropdown dropdown2"></div>
      </div>
    </Layout>
  );
}
