import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { videoDatas } from "../../utils/data";

const HomeSlider = styled.div`
  display: block;

  height: 100%;
  z-index: 2;
  background-color: #e21339;
`;

const LeftContent = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 55vw;
  height: 100%;
  perspective: 400px;

  .videoLoop {
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: 50% 50%;
    overflow: hidden;
  }

  .loop_active {
    opacity: 1;
    visibility: inherit;
    display: block;
    transform: translate(0px, 0px);
    z-index: 3;
  }
  .loop_inactive {
    transform: translate(0%, -5%);
    opacity: 1;
    visibility: inherit;
    display: none;
  }

  @keyframes wipe-in-down {
    from {
      clip-path: inset(0 0 100% 0);
    }
    to {
      clip-path: inset(0 0 0 0);
    }
  }
  /* &:hover {
    animation: 2.5s cubic-bezier(0.25, 1, 0.3, 1) wipe-in-down both;
  } */

  .sliderBullets {
    position: absolute;
    top: calc(50% - 105px);
    left: 30px;
    z-index: 1;
    li {
      position: relative;
      padding: 0 calc(17.14286px + 0.16807vw);
      margin-bottom: 15px;

      cursor: pointer;
    }
    .bullet {
      display: block;
      position: relative;
      width: 2px;
      height: 60px;
      transform-origin: center;
      pointer-events: none;
      .empty {
        display: block;
        width: 2px;
        height: 60px;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #e21339;
        opacity: 0.3;
      }
      .full {
        display: block;
        position: relative;
        width: 2px;
        height: 60px;
        background-color: #e21339;
        transform: scaleY(0);
        transform-origin: center;
      }
    }
  }
`;

const RightContent = styled.div`
  display: block;
  position: absolute;
  top: 0;
  left: 55vw;
  width: 45vw;
  height: 100%;
  background-color: #e21339;
  z-index: -1;

  @keyframes wipe-in-up {
    from {
      clip-path: inset(100% 0 0 0);
    }
    to {
      clip-path: inset(0 0 0 0);
    }
  }
  /* &:hover {
    animation: 2.5s cubic-bezier(0.25, 1, 0.3, 1) wipe-in-up both;
  } */
  .contentDesc {
    position: relative;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    padding-top: 230px;
    margin-left: 8%;
    z-index: 1;
    span {
      color: #fff;
    }
    .numberText {
      font-size: 80px;
      font-weight: 400;
      color: #fff;
      line-height: 1em;
      letter-spacing: -0.48px;
      align-items: center;
      position: relative;
      display: flex;
      .dot {
        width: 25px;
        height: 25px;
        position: relative;
        top: 2px;
        display: inline-block;
        margin-left: 10px;

        border-radius: 30px;
        background-color: #fff;
      }
    }
    .title {
      letter-spacing: -0.48px;
      font-size: 80px;
      font-weight: 400;
      color: #fff;
      line-height: 1em;
    }
  }
`;

export const RLslider = () => {
  const [active, setActive] = useState(videoDatas[0].src[0]);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    setActive(videoDatas[index].src[0]);
  }, [index]);

  return (
    <HomeSlider>
      <LeftContent>
        <ul className="sliderBullets">
          <li>
            <span className="bullet">
              <span className="empty"></span>
              <span className="full"></span>
            </span>
          </li>
          <li>
            <span className="bullet">
              <span className="empty"></span>
              <span className="full"></span>
            </span>
          </li>
          <li>
            <span className="bullet">
              <span className="empty"></span>
              <span className="full"></span>
            </span>
          </li>
        </ul>
        <video
          className="videoLoop loop_active"
          autoPlay
          muted
          playsInline
          loop
        >
          <source
            src="https://redlight.dev/wp-content/themes/RedLight/public/videos/home/home-intro.mp4#t=0.1\"
            type="video/mp4"
          />
          {/* <source src={active ?? ""}
           type="video/mp4" /> */}
        </video>
        {/* <video
          className="videoLoop loop_inactive"
          autoPlay
          muted
          playsInline
          loop
        >
          <source src="https://redlight.dev/wp-content/themes/RedLight/public/videos/home/work-loop.mp4" />
        </video>
        <video
          className="videoLoop loop_inactive"
          autoPlay
          muted
          playsInline
          loop
        >
          <source src="https://redlight.dev/wp-content/themes/RedLight/public/videos/home/contacts-loop.mp4" />
        </video> */}
      </LeftContent>
      <RightContent>
        <div className="contentDesc">
          <span className="numberText">
            <span>1 </span>
            <span className="dot"></span>
          </span>
          <span className="title">
            <div>We are</div>
            <div>Redlight</div>
          </span>
        </div>
      </RightContent>
    </HomeSlider>
  );
};
