import styled from "styled-components";

export const Layout = styled.div`
  height: 100vh;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #fdfcee;
`;

export const Content = styled.div`
  max-width: 400px;
  margin: 80px auto;
  align-items: center;
  min-height: calc(100vh - 160px);
  form {
    width: 100%;
  }
  display: flex;
  align-items: center;
  @media (max-width: 899px) {
    margin: 80px 20px;
    p {
      font-size: 14px;
    }
  }
`;

export const FormHeader = styled.div`
  display: block;
  margin-bottom: 24px;
  text-align: center;

  h1 {
    font-size: 20px;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    color: #1d322d;
    letter-spacing: 0.2em;
  }
`;

export const FormItem = styled.div`
  position: relative;
  margin-bottom: 15px;
  input {
    display: block;
    padding: 12px 14px;
    border-radius: 0;
    border: 1px solid #cbcac0;
    width: 100%;
    line-height: normal;

    transition: border-color 0.1s ease-in-out;
    background: transparent;
  }
  .FormForgotPassword {
    position: absolute;
    right: 12px;
    top: 50%;
    font-size: 12px;
    color: #000;

    transform: translateY(-50%);
    cursor: pointer;
    background-color: transparent;
    border: none;
    &:hover {
      border-color: #404040;
    }
  }
`;

export const FormFooter = styled.div`
  display: block;
  text-align: center;
  margin: 24px 0 0;
  .FormCreate {
    margin: 24px 0 0;
  }
  a {
    margin-left: 5px;
  }
`;

export const RegisterLoginButton = styled.button`
  font-family: "Montserrat", sans-serif;
  font-size: 14px;
  letter-spacing: 0.2em;
  text-transform: uppercase;
  cursor: pointer;
  background-color: transparent;
  border: none;

  padding: 14px 28px;
  margin-top: 20px;

  transition: color 0.45s cubic-bezier(0.785, 0.135, 0.15, 0.86),
    border 0.45s cubic-bezier(0.785, 0.135, 0.15, 0.86);
  &:hover {
    color: #fff;
  }
`;
