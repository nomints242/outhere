import React from "react";
import styled from "styled-components";

const BannerLayout = styled.div`
  position: relative;
  z-index: 0;

  .BannerSlide {
    position: relative;
    display: grid;
    height: 100vh;
    min-height: 720px;
    z-index: 1;
  }
`;

const BannerContent = styled.div`
  position: relative;
  z-index: 2;
  padding: 0 1rem;
  align-self: center;
  text-align: center;
  color: #fdfcee;

  transform: translateY(0);

  h1 {
    margin-bottom: 20px;
    font-size: 36px;
    line-height: 42px;
    font-weight: 300;
    letter-spacing: -0.8px;
  }
  p {
    font-size: 14px;
  }
  .btn {
    display: inline-block;
    border: 1px solid #cbcac0;
    color: #fdfcee;
    line-height: 22px;
    font-weight: 400;
    font-size: 14px;
    white-space: nowrap;
    padding: 19px 48px;
    margin-top: 32px;
    cursor: pointer;
    transition: all 0.4s ease;

    &:hover {
      background-color: #1d322d;
      border-color: #1d322d;
    }
  }
`;

const BannerBackground = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: -1;
  .container {
    position: relative;
    overflow: hidden;
    height: 100%;
    padding: 0;
    z-index: 1;
  }
  .wrapper {
    width: 100%;
    position: relative;
    height: 100%;
    z-index: 1;
  }
  img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;

export function Banner() {
  return (
    <BannerLayout>
      <section>
        <div className="BannerSlide">
          <BannerContent>
            <h1>Materially different culinary goods.</h1>
            <p>
              Exceptional cooking- and dining-ware fashioned for the everyday.
            </p>
            <a className="btn ">Show all</a>
          </BannerContent>
          <BannerBackground>
            <div className="container">
              <div className="wrapper">
                <img
                  className="swiper-lazy lazy entered loaded"
                  data-src="//cdn.shopify.com/s/files/1/2304/7781/files/Lifestyle_-_Shelf_2_02d24327-04ff-4800-8151-bc9013059c2c.jpg?v=1638316551"
                  alt="Lay your table for life."
                  data-ll-status="loaded"
                  src="//cdn.shopify.com/s/files/1/2304/7781/files/Lifestyle_-_Shelf_2_02d24327-04ff-4800-8151-bc9013059c2c.jpg?v=1638316551"
                />
              </div>
            </div>
          </BannerBackground>
        </div>
      </section>
    </BannerLayout>
  );
}
