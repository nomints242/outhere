import React from "react";
import HeaderLight from "../../components/HeaderLight";
import { Layout, Content } from "../../components/RegisterLoginComponents";
import { LoginWidgit } from "../../widgets/account/login";
import Head from "next/head";

export default function login() {
  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <HeaderLight />
      <Layout>
        <Content>
          <LoginWidgit />
        </Content>
      </Layout>
    </>
  );
}
