import React from "react";
import HeaderLight from "../../components/HeaderLight";
import { Layout, Content } from "../../components/RegisterLoginComponents";
import { RegisterWidgit } from "../../widgets/account/register";
import Head from "next/head";

export default function register() {
  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <HeaderLight />
      <Layout>
        <Content>
          <RegisterWidgit />
        </Content>
      </Layout>
    </>
  );
}
