import React from "react";
import Head from "next/head";
import Header from "../components/Header";
import { Banner } from "../components/Banner";

export default function index() {
  return (
    <>
      <Head>
        <title>Material Kitchen</title>
        <meta property="og:title" content="MaterialKitchen" />
        <meta property="og:description" content="" />
        <meta
          property="og:image"
          content="http://euro-travel-example.com/thumbnail.jpg"
        />
        <meta property="og:url" content="#" />
      </Head>
      <Header />

      <Banner />
    </>
  );
}
