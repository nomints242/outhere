import React from "react";
import styled from "styled-components";
import { RLslider } from "../components/redlight/RLslider";

export default function Redlight() {
  return (
    <>
      <RLslider />
    </>
  );
}
