import {
  FormHeader,
  FormFooter,
  FormItem,
  RegisterLoginButton,
} from "../../../components/RegisterLoginComponents";
import useRegister from "./useRegister";
import { RegisterForm } from "./useRegister";

export const RegisterWidgit = () => {
  const { submit, register, handleSubmit } = useRegister();
  return (
    <form onSubmit={handleSubmit(submit)}>
      <FormHeader>
        <h1>Register</h1>
        <p>Please fill in the information below:</p>
      </FormHeader>

      <FormItem>
        <input
          type="tel"
          className="Form__Input"
          {...register("phonenumber", { required: true })}
          placeholder="Phone Number"
          aria-label="phonenumber"
        />
      </FormItem>
      <FormItem>
        <input
          type="password"
          className="Form__Input"
          {...register("password", { required: true })}
          placeholder="Password"
          aria-label="Password"
        />
      </FormItem>
      <FormItem>
        <input
          type="password"
          className="Form__Input"
          {...register("password2", { required: true })}
          placeholder="confirm Password"
          aria-label="password2"
        />
      </FormItem>
      <FormFooter>
        <RegisterLoginButton type="submit">
          Create my account
        </RegisterLoginButton>
      </FormFooter>
    </form>
  );
};
