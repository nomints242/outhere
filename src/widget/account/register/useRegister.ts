import sdk from "../../../sdk";
import { useForm } from "react-hook-form";
import { useCallback } from "react";
import { Role } from "../../../graphql";
export type RegisterForm = {
  password: string;
  password2: string;
  phonenumber: string;
};

const useRegister = () => {
  const { register, handleSubmit } = useForm<RegisterForm>();

  const submit = useCallback((values: RegisterForm) => {
    if (values.password === values.password2) {
      sdk
        .register({
          user: {
            password: values.password,
            password2: values.password2,
            phonenumber: values.phonenumber,
            role: Role.User,
          },
        })
        .then(() => console.log("succuss"))
        .catch((err) => console.log(err));
    }
  }, []);
  return { submit, register, handleSubmit };
};

export default useRegister;
