import {
  FormHeader,
  FormFooter,
  FormItem,
  RegisterLoginButton,
} from "../../../components/RegisterLoginComponents";

export const LoginWidgit = () => {
  return (
    <form
      className="Loginform"
      action="/account"
      method="post"
      name="create_customer"
    >
      <FormHeader>
        <h1>LOGIN</h1>
        <p>Please enter your e-mail and password:</p>
      </FormHeader>
      <FormItem>
        <input
          type="email"
          className="Form__Input"
          name="customer[email]"
          placeholder="Email"
          aria-label="Email"
        />
      </FormItem>
      <FormItem>
        <input
          type="password"
          className="Form__Input"
          name="customer[password]"
          placeholder="Password"
          aria-label="Password"
        />

        <button className="FormForgotPassword">Forgot password?</button>
      </FormItem>
      <FormFooter>
        <RegisterLoginButton type="submit">Login</RegisterLoginButton>
        <div className="FormCreate">
          <span>Don't have an account?</span>
          <a href="/account/register">Create one</a>
        </div>
      </FormFooter>
    </form>
  );
};
