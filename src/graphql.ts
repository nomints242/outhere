import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type AddUserInput = {
  password: Scalars['String'];
  password2: Scalars['String'];
  phonenumber: Scalars['String'];
  role: Role;
};

export type Auth = {
  __typename?: 'Auth';
  token: Scalars['String'];
  user: User;
};

export type EditUserInput = {
  password?: InputMaybe<Scalars['String']>;
  phonenumber?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Role>;
};

export type GetUsersResult = {
  __typename?: 'GetUsersResult';
  totalItems: Scalars['Int'];
  totalPages: Scalars['Int'];
  users?: Maybe<Array<User>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addUser: Scalars['Boolean'];
  deleteUser: Scalars['Boolean'];
  editUser: Scalars['Boolean'];
  login: Auth;
  register: Scalars['Boolean'];
};


export type MutationAddUserArgs = {
  user: AddUserInput;
};


export type MutationDeleteUserArgs = {
  _id: Scalars['ID'];
};


export type MutationEditUserArgs = {
  _id: Scalars['ID'];
  user: EditUserInput;
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  phonenumber: Scalars['String'];
};


export type MutationRegisterArgs = {
  user: AddUserInput;
};

export type Query = {
  __typename?: 'Query';
  getUser: User;
  getUsers: GetUsersResult;
};


export type QueryGetUserArgs = {
  _id: Scalars['ID'];
};


export type QueryGetUsersArgs = {
  limit: Scalars['Int'];
  page: Scalars['Int'];
};

export enum Role {
  Admin = 'ADMIN',
  User = 'USER'
}

export type User = {
  __typename?: 'User';
  _id?: Maybe<Scalars['ID']>;
  phonenumber: Scalars['String'];
  role: Role;
};

export type AddUserMutationVariables = Exact<{
  user: AddUserInput;
}>;


export type AddUserMutation = { __typename?: 'Mutation', addUser: boolean };

export type RegisterMutationVariables = Exact<{
  user: AddUserInput;
}>;


export type RegisterMutation = { __typename?: 'Mutation', addUser: boolean };


export const AddUserDocument = gql`
    mutation addUser($user: AddUserInput!) {
  addUser(user: $user)
}
    `;
export const RegisterDocument = gql`
    mutation register($user: AddUserInput!) {
  addUser(user: $user)
}
    `;

export type SdkFunctionWrapper = <T>(action: (requestHeaders?:Record<string, string>) => Promise<T>, operationName: string) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = (action, _operationName) => action();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    addUser(variables: AddUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AddUserMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<AddUserMutation>(AddUserDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'addUser');
    },
    register(variables: RegisterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<RegisterMutation> {
      return withWrapper((wrappedRequestHeaders) => client.request<RegisterMutation>(RegisterDocument, variables, {...requestHeaders, ...wrappedRequestHeaders}), 'register');
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;