export const videoDatas = [
  {
    id: 0,
    src: "https://redlight.dev/wp-content/themes/RedLight/public/videos/home/home-intro.mp4#t=0.1",
  },
  {
    id: 1,
    src: "https://redlight.dev/wp-content/themes/RedLight/public/videos/home/work-loop.mp4",
  },
  {
    id: 2,
    src: "https://redlight.dev/wp-content/themes/RedLight/public/videos/home/contacts-loop.mp4",
  },
];
